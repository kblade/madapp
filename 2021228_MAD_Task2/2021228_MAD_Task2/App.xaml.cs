﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using _2021228_MAD_Task2.Views;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace _2021228_MAD_Task2
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();


            MainPage = new NavigationPage(new HomePage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
