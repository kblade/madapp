﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace _2021228_MAD_Task2.Views
{
	public class HomePage : ContentPage
	{
		public HomePage ()
		{
            this.Title = "Rats Apps!";
            StackLayout stackLayout = new StackLayout();

            Button button = new Button(); //Add Button
            button.Text = "Add Family or Friends";
            button.Clicked += Button_Clicked;
            stackLayout.Children.Add(button);

            button = new Button(); //Get Button
            button.Text = "Get list of Family and Friends";
            button.Clicked += Button_Get_Clicked;
            stackLayout.Children.Add(button);

            button = new Button(); //Instructions Button
            button.Text = "Instructions";
            button.Clicked += Button_Instructions_Clicked;
            stackLayout.Children.Add(button);

            Content = stackLayout;
		}

        private async void Button_Get_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new GetListPage()); //Takes it to the add page
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new FamilyFriendsPage()); //Takes it to the add page
        }
        private async void Button_Instructions_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new InstructionsPage()); //takes it to the instructions page
        }
    }
}