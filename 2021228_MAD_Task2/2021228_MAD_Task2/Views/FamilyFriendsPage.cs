﻿using _2021228_MAD_Task2.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace _2021228_MAD_Task2.Views
{
	public class FamilyFriendsPage : ContentPage
	{
        //This is where we hold the entries of the user and also the buttons of which we want
        private Entry Name;
        private Entry Relation;
        private Button SaveButton;

        string _dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "myDB.db3");
		public FamilyFriendsPage ()
		{
            this.Title = "Add Family or Friend";

            StackLayout stackLayout = new StackLayout();

            //This is getting the name of the friend or family member
            Name = new Entry();
            Name.Keyboard = Keyboard.Text;
            Name.Placeholder = "Name";
            stackLayout.Children.Add(Name);

            //This is getting what relation they are to you
            Relation = new Entry();
            Relation.Keyboard = Keyboard.Text;
            Relation.Placeholder = "Relation";
            stackLayout.Children.Add(Relation);

            //This button saves the data gicen from the entries before
            SaveButton = new Button();
            SaveButton.Text = "Add";
            SaveButton.Clicked += SaveButton_Clicked;
            stackLayout.Children.Add(SaveButton);

            Content = stackLayout;
        }

        private async void SaveButton_Clicked(object sender, EventArgs e)
        {
            //This is saving the information to the SQLite database
            var db = new SQLiteConnection(_dbPath);
            db.CreateTable<Family>();

            var maxPk = db.Table<Family>().OrderByDescending(c => c.Id).FirstOrDefault();

            Family family = new Family()
            {
                Id = (maxPk == null ? 1 : maxPk.Id + 1),
                Name = Name.Text,
                Relation = Relation.Text
            };
            db.Insert(family);
            //This is showing validation that the data has saved
            await DisplayAlert(null, family.Name + " Saved", "OK");
            await Navigation.PopAsync();
        }
    }
}