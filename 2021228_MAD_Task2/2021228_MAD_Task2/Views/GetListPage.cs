﻿using SQLite;
using _2021228_MAD_Task2.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace _2021228_MAD_Task2.Views
{
	public class GetListPage : ContentPage

	{
        private ListView listView;
        string _dbPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "myDB.db3");
        public GetListPage ()
		{
            listView = new ListView
            {
                ItemTemplate = new DataTemplate(() =>
                {
                    Label NameLabel = new Label();
                    NameLabel.SetBinding(Label.TextProperty, "Name");
                    Label RelationLabel = new Label();
                    RelationLabel.SetBinding(Label.TextProperty, "Relation");
                    RelationLabel.TextColor = Color.FromHex("#ff0000");

                    return new ViewCell
                    {
                        View = new StackLayout
                        {
                            Padding = new Thickness(10, 2),
                            Orientation = StackOrientation.Horizontal,
                            Children =
                            {
                                new StackLayout
                                {
                                    VerticalOptions = LayoutOptions.Center,
                                    Spacing = 0,
                                    Children =
                                    {
                                        NameLabel,
                                        RelationLabel,
                                    }
                                }
                            }
                        }
                    };
                })
            };
            this.Title = "All Family and Friends";
            var db = new SQLiteConnection(_dbPath);

            StackLayout stackLayout = new StackLayout();

            listView.ItemsSource = db.Table<Family>().OrderBy(x => x.Name).ToList();
            stackLayout.Children.Add(listView);

            Content = stackLayout;

			
		}
	}
}