﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace _2021228_MAD_Task2.Views
{
	public class InstructionsPage : ContentPage
	{
		public InstructionsPage ()
		{
           

            var layout = new StackLayout { Padding = new Thickness(5, 10) };
            var label = new Label { Text = "Add your family and friends to this app to help remember their details!",  FontSize = 30 };
            layout.Children.Add(label);
            this.Content = layout;


        }
	}
}