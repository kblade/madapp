﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace _2021228_MAD_Task2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }
    }
}