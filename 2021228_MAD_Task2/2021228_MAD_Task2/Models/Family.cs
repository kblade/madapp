﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace _2021228_MAD_Task2.Models
{
    public class Family
    {
        
        [PrimaryKey]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Relation { get; set; }
    }
}
