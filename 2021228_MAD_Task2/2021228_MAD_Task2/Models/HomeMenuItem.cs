﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _2021228_MAD_Task2.Models
{
    public enum MenuItemType
    {
        Browse,
        About
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
